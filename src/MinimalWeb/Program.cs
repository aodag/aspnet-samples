using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MinimalWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build();

            var host = new WebHostBuilder()
                .UseConfiguration(config)
                .UseStartup<Startup>()
                .UseKestrel()
                .Build();
            host.Run();
        }
    }
}
