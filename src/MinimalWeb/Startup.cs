using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace MinimalWeb
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            app.Run(c => {
                return c.Response.WriteAsync("Hello, world!");
            });
        }
    }
}
